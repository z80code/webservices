package by.epam.training.logic.impl;

import by.epam.training.logic.IChecker;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class DoubleValueCheckerTest {

	private IChecker<Double> checker = new DoubleValueChecker();

	@Test
	public void checkMoreThenZero() throws Exception {
		Assert.assertFalse(checker.check(1D));
	}

	@Test
	public void checkLowerThenZero() throws Exception {
		Assert.assertTrue(checker.check(-1D));
	}

	@Test
	public void checkEqualZero() throws Exception {
		Assert.assertFalse(checker.check(0D));
	}
}