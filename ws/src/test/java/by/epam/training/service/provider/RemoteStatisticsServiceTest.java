package by.epam.training.service.provider;

import by.epam.training.client.impl.StatisticsClient;
import by.epam.training.wsdl.GetStatisticsResponse;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Collection;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RemoteStatisticsServiceTest {
	@Mock
	private
	RemoteStatisticsService statisticsService = mock(RemoteStatisticsService.class);
	@Mock
	private StatisticsClient statisticsClient = mock(StatisticsClient.class);
	@SuppressWarnings("unchecked")
	@Mock
	private Collection<Double> mockCollection=(Collection<Double>) mock(Collection.class);;
	@Mock
	private GetStatisticsResponse response = new GetStatisticsResponse();

	@Test
	public void getStatistics() throws Exception {

		when(statisticsService.getStatistics(mockCollection)).thenReturn(response);
		statisticsService.getStatistics(mockCollection);
		verify(statisticsService).getStatistics(mockCollection);
	}
}