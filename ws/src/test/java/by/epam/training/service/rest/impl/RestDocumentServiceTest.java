package by.epam.training.service.rest.impl;

import by.epam.training.entity.Chapter;
import by.epam.training.entity.Document;
import by.epam.training.service.impl.DocumentServiceImpl;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class RestDocumentServiceTest {


	private DocumentServiceImpl documentService = mock(DocumentServiceImpl.class);

	private RestDocumentService restDocumentService = mock(RestDocumentService.class);

	@Test
	public void getAllDocuments() throws Exception {
		restDocumentService.getAllDocuments();
		verify(restDocumentService).getAllDocuments();

	}

	@Test
	public void getDocument() throws Exception {
		restDocumentService.getDocument(2L);
		verify(restDocumentService).getDocument(2L);
	}

	@Test
	public void updateDocument() throws Exception {
		Document document = new Document();
		restDocumentService.updateDocument(2L, document);
		verify(restDocumentService).updateDocument(2L, document);
	}

	@Test
	public void putDocument() throws Exception {
		Document document = new Document();
		restDocumentService.putDocument(2L, document);
		verify(restDocumentService).putDocument(2L, document);
	}

	@Test
	public void deleteDocument() throws Exception {
		restDocumentService.deleteDocument(2L);
		verify(restDocumentService).deleteDocument(2L);
	}

	@Test
	public void updateChapter() throws Exception {
		Chapter chapter= new Chapter();
		restDocumentService.updateChapter(0L,0L,chapter);
		verify(restDocumentService).updateChapter(0L,0L,chapter);
	}

}