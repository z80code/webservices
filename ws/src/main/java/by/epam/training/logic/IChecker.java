package by.epam.training.logic;

public interface IChecker<T> {
	boolean check(T value);
}
