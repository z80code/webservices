package by.epam.training.exceptions;

import by.epam.training.exception.DocumentException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DocumentExceptionMapper implements ExceptionMapper<DocumentException> {

	@Override
	public Response toResponse(DocumentException exception) {
		return Response.status(Response.Status.FORBIDDEN).entity(exception.getMessage()).build();
	}
}
