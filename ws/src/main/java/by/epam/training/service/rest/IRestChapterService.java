package by.epam.training.service.rest;

import by.epam.training.entity.Chapter;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/documents")
@Consumes(MediaType.APPLICATION_XML)
@Produces(MediaType.APPLICATION_JSON)
public interface IRestChapterService {
	@PUT
	@Path("/{documentId}/chapters/{chapterId}")
	Response updateChapter(@PathParam("documentId") Long documentId, @PathParam("chapterId") Long chapterId, Chapter chapter);
}
