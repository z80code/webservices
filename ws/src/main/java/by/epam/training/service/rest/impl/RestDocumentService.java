package by.epam.training.service.rest.impl;

import by.epam.training.entity.Chapter;
import by.epam.training.entity.Document;
import by.epam.training.service.rest.IRestChapterService;
import by.epam.training.service.rest.IRestDocumentService;
import by.epam.training.service.impl.DocumentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.Collection;

// #Task3
//	Web Service should consume objects in XML format and return objects in JSON format.
//	Implement method for updating particular Chapter of Document.
//	In case of deleting all Document’s Chapters – 403 status code with appropriate message
// should be set to response.

@Component
public class RestDocumentService implements IRestDocumentService, IRestChapterService {

	private DocumentServiceImpl documentService;

	public RestDocumentService() {
	}

	@Autowired
	public RestDocumentService(DocumentServiceImpl documentService) {
		this.documentService = documentService;
	}

	/**
	 * Method GET
	 * Returns list of all documents, return status is 200
	 *
	 * @return
	 */
	@Override
	public Response getAllDocuments() {
		Collection<Document> listDocuments = documentService.getAll();
		return Response.status(Response.Status.OK).entity(listDocuments).build();
	}

	/**
	 * Method GET
	 * Returns Document with id=1, return status is 200
	 *
	 * @return
	 */
	@Override
	public Response getDocument(Long id) {
		Document document = documentService.read(id);
		return Response.ok().entity(document).build();
	}

	/**
	 * Method POST
	 * Creates new Document from Request, return status is 201
	 *
	 * @return
	 */
	@Override
	public Response addDocument(Document document) {
		Document createdDocument = documentService.create(document);
		return Response.status(Response.Status.CREATED).entity(createdDocument).build();
	}

	/**
	 * Method PATCH
	 * Modifies Document with id with Document from Request, return status is 204
	 *
	 * @return
	 */
	@Override
	public Response updateDocument(Long id, Document document) {
		documentService.update(id, document);
		return Response.status(Response.Status.NO_CONTENT).build();
	}

	/**
	 * Method PUT
	 * no id? => as POST
	 * Creates new Document from Request, return status is 201
	 *
	 * @return
	 */
	@Override
	public Response putDocument(Document document) {
		return addDocument(document);
	}

	/**
	 * Method PUT
	 * Updates Document with 'id' and with 'document' from Request, return status is 204
	 *
	 * @return
	 */
	@Override
	public Response putDocument(Long id, Document document) {
		Document createdDocument = documentService.put(id, document);
		return Response.status(Response.Status.CREATED).entity(createdDocument).build();
	}

	/**
	 * Method DELETE
	 *
	 * @return
	 */
	@Override
	public Response deleteDocument(Long id) {
		documentService.delete(id);
		return Response.status(Response.Status.NO_CONTENT).build();
	}

	/**
	 * Method PATCH
	 *
	 * @return
	 */
	@Override
	public Response updateChapter(Long documentId, Long chapterId, Chapter chapter) {
		documentService.updateChapter(documentId, chapterId, chapter);
		return Response.status(Response.Status.NO_CONTENT).build();
	}
}