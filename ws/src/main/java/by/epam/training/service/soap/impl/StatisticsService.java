package by.epam.training.service.soap.impl;

import by.epam.training.logic.IChecker;
import by.epam.training.service.provider.RemoteStatisticsService;
import by.epam.training.service.soap.IStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import java.util.List;

/**
 * It`s StatisticsService class implements IStatisticsService.
 * This one have two methods:
 * 'testService()'
 * and
 * 'getAverage(List<Double> values)'
 */
@Component
@WebService(
		endpointInterface = "by.epam.training.service.soap.IStatisticsService",
		serviceName = "StatisticsService")
public class StatisticsService implements IStatisticsService {

	/**
	 * A Test method for service check.
	 *
	 * @return a string witch always contents "Success!" value.
	 */

	private IChecker<Double> checker;
	private RemoteStatisticsService remoteStatisticsService;

	public StatisticsService() {
	}


	@Autowired
	public StatisticsService(IChecker<Double> checker, RemoteStatisticsService remoteStatisticsService) {
		this.checker = checker;
		this.remoteStatisticsService = remoteStatisticsService;
	}

	@Override
	public String testService() {
		return "Success!";
	}

	/**
	 * A Method for receiving an average value of List.
	 *
	 * @param values List<Double> values.
	 * @return An average value.
	 */

	@Override
	public Double getAverage(List<Double> values) {

		double result = remoteStatisticsService.getStatistics(values).getAverage();
		if (checker.check(result)) {
			throw new RuntimeException("Sum of all numbers can`t be lower then 0.");
		}
		return result;
	}
}
