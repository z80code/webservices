package by.epam.training.service.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IStatisticsService {
	@WebMethod
	String testService();

	@WebMethod
	@WebResult(name="average")
	Double getAverage(@WebParam(name = "double") List<Double> values);
}
