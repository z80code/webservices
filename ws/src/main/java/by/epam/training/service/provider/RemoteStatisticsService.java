package by.epam.training.service.provider;

import by.epam.training.client.IStatisticsClient;
import by.epam.training.client.impl.StatisticsClient;
import by.epam.training.wsdl.GetStatisticsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

/**
 * This class contains methods of the mathematics.
 */
@Component
public class RemoteStatisticsService implements IStatisticsClient<Double> {

	private StatisticsClient statisticsClient;

	public RemoteStatisticsService() {
	}

	@Autowired
	public RemoteStatisticsService(StatisticsClient statisticsClient) {
		this.statisticsClient = statisticsClient;
	}


	@Override
	public GetStatisticsResponse getStatistics(Collection<Double> values) {
		return statisticsClient.getStatistics(values);
	}
}
