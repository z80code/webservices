package by.epam.training.service.rest;

import by.epam.training.entity.Document;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/documents")
@Consumes(MediaType.APPLICATION_XML)
@Produces(MediaType.APPLICATION_JSON)
public interface IRestDocumentService {

	@GET
	@Path("/")
	Response getAllDocuments();

	@GET
	@Path("/{documentId}")
	Response getDocument(@PathParam("documentId") Long id);

	@POST
	@Path("/")
	Response addDocument(Document document);

	@PUT
	@Path("/")
	Response putDocument(Document document);

	@PUT
	@Path("/{documentId}")
	Response putDocument(@PathParam("documentId") Long id, Document document);

	@PATCH
	@Path("/{documentId}")
	Response updateDocument(@PathParam("documentId") Long id, Document document);

	@DELETE
	@Path("/{documentId}")
	Response deleteDocument(@PathParam("documentId") Long id);
}
