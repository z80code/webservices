package by.epam.training.client.impl;

import by.epam.training.wsdl.GetStatisticsResponse;
import org.junit.Assert;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

public class StatisticsClientTest {
	@org.junit.Test
	public void getStatistics() throws Exception {

		List<Double> collection = Arrays.asList(3.0, 5.0);

		StatisticsClient client = new StatisticsClient();

		GetStatisticsResponse response = client.getStatistics(collection);

		double expected = (3.0 + 5.0) / 2;

		Assert.assertEquals(expected, response.getAverage(), 0);
	}

}
