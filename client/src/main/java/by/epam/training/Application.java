
package by.epam.training;

import by.epam.training.client.impl.StatisticsClient;
import by.epam.training.wsdl.GetStatisticsResponse;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;

/**
 * A test app for the client part.
 * It`s must be removed.
 */
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	@Bean
	CommandLineRunner lookup(StatisticsClient statisticsClient) {
		return args -> {
			List<Double> list = Arrays.asList(2.5,3.5);
			GetStatisticsResponse response = statisticsClient.getStatistics(list);
			System.err.println(response.getAverage());
		};
	}
}
