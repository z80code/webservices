package by.epam.training.client.impl;

import by.epam.training.client.IStatisticsClient;
import by.epam.training.wsdl.ArrayOfDouble;
import by.epam.training.wsdl.GetStatistics;
import by.epam.training.wsdl.GetStatisticsResponse;
import by.epam.training.wsdl.ObjectFactory;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.util.Collection;

/**
 * A class implement`s IStatisticsClient<T> interface for the GetStatistics service
 * by url "http://www.webservicex.net/statistics.asmx".
 */
@Component
public class StatisticsClient extends WebServiceGatewaySupport implements IStatisticsClient<Double> {

	private static final String URL_SOAP_ACTION_CALLBACK = "http://www.webserviceX.NET/GetStatistics";
	private static final String URI_TO_SEND_THE_MESSAGE = "http://www.webservicex.net/statistics.asmx";

	private static final String CONTEXT_PATH = "by.epam.training.wsdl";

	public StatisticsClient() {
		super();
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath(CONTEXT_PATH);
		setDefaultUri(URI_TO_SEND_THE_MESSAGE);
		setMarshaller(marshaller);
		setUnmarshaller(marshaller);
	}

	/**
	 * A method of service to get statistics the client`s class.
	 *
	 * @param values Collection of values.
	 * @return A statistics response contains different statistics data.
	 */
	@Override
	public GetStatisticsResponse getStatistics(Collection<Double> values) {

		ObjectFactory factory = new ObjectFactory();
		GetStatistics request = factory.createGetStatistics();
		ArrayOfDouble arrayOfDouble = factory.createArrayOfDouble();
		arrayOfDouble.getDouble().addAll(values);
		request.setX(arrayOfDouble);
		WebServiceTemplate webServiceTemplate = getWebServiceTemplate();
		SoapActionCallback soapActionCallback = new SoapActionCallback(URL_SOAP_ACTION_CALLBACK);

		return (GetStatisticsResponse) webServiceTemplate
				.marshalSendAndReceive(
						URI_TO_SEND_THE_MESSAGE,
						request,
						soapActionCallback);
	}
}
