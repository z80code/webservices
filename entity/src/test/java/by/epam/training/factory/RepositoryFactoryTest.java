package by.epam.training.factory;

import by.epam.training.entity.Document;
import by.epam.training.repository.IRepository;
import org.junit.Assert;
import org.junit.Test;

public class RepositoryFactoryTest {

	@Test
	public void getDocumentRepositoryFactory() throws Exception {
		RepositoryFactory factory =	new RepositoryFactory();

		Assert.assertNotNull(factory.getDocumentRepository());
		Assert.assertTrue(factory.getDocumentRepository() instanceof IRepository);
	}

}