package by.epam.training.repository;

import by.epam.training.entity.Document;
import by.epam.training.entity.Chapter;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;

public class MemoryDocumentRepositoryImplTest {
	@Test
	public void getAll() throws Exception {
		MemoryDocumentRepositoryImpl repository = new MemoryDocumentRepositoryImpl();
		Collection<Document> collection =repository.getAll();
		Assert.assertNotNull(collection);
	}

	@Test
	public void getIndexesCounter() throws Exception {
		MemoryDocumentRepositoryImpl repository = new MemoryDocumentRepositoryImpl();
		AtomicLong index = repository.getIndexesCounter();
		AtomicLong expected = new AtomicLong(0);
		Assert.assertEquals(expected.get(), index.get());
	}

	@Test
	public void setIndexesCounter() throws Exception {
		MemoryDocumentRepositoryImpl repository = new MemoryDocumentRepositoryImpl();

		AtomicLong expected = new AtomicLong(5);

		AtomicLong index = repository.getIndexesCounter();
		Assert.assertEquals(0, index.get());
		repository.setIndexesCounter(expected);
		AtomicLong newIndex = repository.getIndexesCounter();
		Assert.assertEquals(expected, newIndex);
	}

	@Test
	public void create() throws Exception {
		IRepository<Document> repository = new MemoryDocumentRepositoryImpl();

		Document document = new Document(
				3L,
				"Report",
				new ArrayList<Chapter>() {{
					add(new Chapter(1L, 4, 6));
				}});

		repository.create(document);
		Assert.assertEquals(document, repository.read(0L));
	}

	@Test
	public void createFirstDocWithZeroId() throws Exception {
		IRepository<Document> repository = new MemoryDocumentRepositoryImpl();

		Document document = new Document(
				3L,
				"Report",
				new ArrayList<Chapter>() {{
					add(new Chapter(1L, 4, 6));
				}});

		repository.create(document);
		Assert.assertNotNull(repository.read(0L));
	}

	@Test
	public void createChangedId() throws Exception {
		IRepository<Document> repository = new MemoryDocumentRepositoryImpl();

		Document document = new Document(
				3L,
				"Report",
				new ArrayList<Chapter>() {{
					add(new Chapter(1L, 4, 6));
				}});

		repository.create(document);
		Assert.assertNull(repository.read(3L));
	}


	@Test
	public void read() throws Exception {
		IRepository<Document> repository = new MemoryDocumentRepositoryImpl();

		Document document = new Document(
				3L,
				"Report",
				new ArrayList<Chapter>() {{
					add(new Chapter(1L, 4, 6));
				}});
		Document document1 = new Document(
				2L,
				"Report1",
				new ArrayList<Chapter>() {{
					add(new Chapter(2L, 8, 4));
				}});
		repository.create(document);
		repository.create(document1);
		Assert.assertEquals(document, repository.read(0L));
		Assert.assertEquals(document1, repository.read(1L));
	}

	@Test
	public void update() throws Exception {
		IRepository<Document> repository = new MemoryDocumentRepositoryImpl();

		Document document = new Document(
				3L,
				"Report",
				new ArrayList<Chapter>() {{
					add(new Chapter(1L, 4, 6));
				}});
		Document document1 = new Document(
				0L,
				"Report1",
				new ArrayList<Chapter>() {{
					add(new Chapter(2L, 8, 4));
				}});
		repository.create(document);
		Assert.assertEquals(document, repository.read(0L));
		repository.update(document1);
		Assert.assertEquals(document1, repository.read(0L));
	}

	@org.junit.Test
	public void delete() throws Exception {
		IRepository<Document> repository = new MemoryDocumentRepositoryImpl();

		Document document = new Document(
				3L,
				"Report",
				new ArrayList<Chapter>() {{
					add(new Chapter(1L, 4, 6));
				}});
		repository.create(document);
		Assert.assertEquals(document, repository.read(0L));
		repository.delete(0L);
		Assert.assertNull(repository.read(0L));
	}
}
