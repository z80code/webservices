package by.epam.training.logic.impl;

import by.epam.training.entity.Chapter;
import by.epam.training.entity.Document;
import by.epam.training.logic.IUpdater;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class ChapterUpdateImplTest {


	Chapter oldChapter =
							new Chapter(
									0L,
									1,
									2
							);

	Chapter newChapter =
							new Chapter(
									3L,
									4,
									5
							);

	Chapter newChapterWithChapterNumberIsNull =
			new Chapter(
					3L,
					null,
					5
			);

	Chapter newChapterWithNumberOfPagesIsNull =
			new Chapter(
					3L,
					3,
					null
			);

	@Test(expected = IllegalArgumentException.class)
	public void updateNullArgOldValueException() throws Exception {
		IUpdater<Chapter> updater = new ChapterUpdateImpl();
		updater.update(null, newChapter);
	}

	@Test(expected = IllegalArgumentException.class)
	public void updateNullArgNewValueException() throws Exception {
		IUpdater<Chapter> updater = new ChapterUpdateImpl();
		updater.update(oldChapter, null);
	}

	@Test
	public void updateResultChapterId() throws Exception {
		IUpdater<Chapter> updater = new ChapterUpdateImpl();
		Chapter resultChapter = updater.update(oldChapter, newChapter);
		Long expected = oldChapter.getId();
		Assert.assertEquals(expected, resultChapter.getId());
	}

	@Test
	public void updateResultChapterNumberIfNewChapterHaveChapterNumberNull() throws Exception {
		IUpdater<Chapter> updater = new ChapterUpdateImpl();
		Chapter resultChapter = updater.update(oldChapter, newChapterWithChapterNumberIsNull);
		Integer expected = oldChapter.getChapterNumber();
		Assert.assertEquals(expected, resultChapter.getChapterNumber());
	}

	@Test
	public void updateResultChapterNumberIfNewChapterHaveNumberOfPagesNull() throws Exception {
		IUpdater<Chapter> updater = new ChapterUpdateImpl();
		Chapter resultChapter = updater.update(oldChapter, newChapterWithNumberOfPagesIsNull);
		Integer expected = oldChapter.getNumberOfPages();
		Assert.assertEquals(expected, resultChapter.getNumberOfPages());
	}

	@Test
	public void updateResultChapterNumberIfNewChapterHaveChapterNumberNotNull() throws Exception {
		IUpdater<Chapter> updater = new ChapterUpdateImpl();
		Chapter resultChapter = updater.update(oldChapter, newChapter);
		Integer expected = newChapter.getChapterNumber();
		Assert.assertEquals(expected, resultChapter.getChapterNumber());
	}

	@Test
	public void updateResultChapterNumberIfNewChapterHaveNumberOfPagesNotNull() throws Exception {
		IUpdater<Chapter> updater = new ChapterUpdateImpl();
		Chapter resultChapter = updater.update(oldChapter, newChapter);
		Integer expected = newChapter.getNumberOfPages();
		Assert.assertEquals(expected, resultChapter.getNumberOfPages());
	}
}