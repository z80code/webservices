package by.epam.training.logic.impl;

import by.epam.training.entity.Chapter;
import by.epam.training.entity.Document;
import by.epam.training.entity.Document;
import by.epam.training.exception.DocumentAccessException;
import by.epam.training.logic.IUpdater;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class DocumentUpdaterImplTest {

	Document oldDocument =
			new Document(
					0L,
					"Report",
					Arrays.asList(
							new Chapter(
									0L,
									1,
									20
							),
							new Chapter(
									1L,
									2,
									35
							),
							new Chapter(
									2L,
									3,
									15
							)

					)
			);

	Document newDocument =
			new Document(
					1L,
					"SubReport",
					Arrays.asList(
							new Chapter(
									3L,
									1,
									16
							),
							new Chapter(
									4L,
									2,
									62
							),
							new Chapter(
									5L,
									3,
									23
							)

					)
			);

	Document newDocumentWithIdIsNull =
			new Document(
					null,
					"SubReport",
					Arrays.asList(
							new Chapter(
									3L,
									1,
									16
							),
							new Chapter(
									4L,
									2,
									62
							),
							new Chapter(
									5L,
									3,
									23
							)

					)
			);
	Document newDocumentWithNameIsNull =
			new Document(
					1L,
					null,
					Arrays.asList(
							new Chapter(
									3L,
									1,
									16
							),
							new Chapter(
									4L,
									2,
									62
							),
							new Chapter(
									5L,
									3,
									23
							)

					)
			);

	Document newDocumentWithChapterIsNull =
			new Document(
					1L,
					"SubReport",
					null
			);

	IUpdater<Document> updater = new DocumentUpdaterImpl();

	@Test(expected = IllegalArgumentException.class)
	public void updateNullArgOldValueException() throws Exception {
		updater.update(null, newDocument);
	}

	@Test(expected = IllegalArgumentException.class)
	public void updateNullArgNewValueException() throws Exception {
		updater.update(oldDocument, null);
	}

	@Test
	public void updateResultDocumentId() throws Exception {
		Document resultDocument = updater.update(oldDocument, newDocument);
		Long expected = oldDocument.getId();
		Assert.assertEquals(expected, resultDocument.getId());
	}

	@Test
	public void updateResultDocumentNameIfNewDocumentHaveDocumentNameIsNull() throws Exception {
		Document resultDocument = updater.update(oldDocument, newDocumentWithNameIsNull);
		String expected = oldDocument.getName();
		Assert.assertEquals(expected, resultDocument.getName());
	}

	@Test
	public void updateResultDocumentChaptersIfNewDocumentHaveDocumentChaptersIsNull() throws Exception {
		Document resultDocument = updater.update(oldDocument, newDocumentWithChapterIsNull);
		List<Chapter> expected = oldDocument.getChapters();
		Assert.assertEquals(expected, resultDocument.getChapters());
	}

	@Test
	public void updateResultDocumentNameIfNewDocumentHaveDocumentNameNotNull() throws Exception {
		Document resultDocument = updater.update(oldDocument, newDocument);
		String expected = newDocument.getName();
		Assert.assertEquals(expected, resultDocument.getName());
	}

	@Test
	public void updateResultDocumentChaptersIfNewDocumentHaveChaptersNotNull() throws Exception {
		Document resultDocument = updater.update(oldDocument, newDocument);
		List<Chapter> expected = newDocument.getChapters();
		Assert.assertEquals(expected, resultDocument.getChapters());
	}
}