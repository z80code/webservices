package by.epam.training.logic.impl;

import by.epam.training.entity.Chapter;
import by.epam.training.entity.Document;
import by.epam.training.exception.DocumentAccessException;
import by.epam.training.logic.IValidator;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class DocumentValidatorTest {
	private static final Document goodDocument = new Document(
			0L,
			"Document",
			Arrays.asList(
					new Chapter(
							3L,
							1,
							38
					),
					new Chapter(
							4L,
							2,
							17
					),
					new Chapter(
							5L,
							3,
							41
					)
			)
	);
	private static final Document nullChaptersDocument = new Document(
			0L,
			"TestDocument",
			null
	);
	private static final Document emptyChaptersDocument = new Document(
			0L,
			"TestDocument",
			new ArrayList<>()
	);

	@Test
	public void validate() throws Exception {
	IValidator<Document> documentIValidator = new DocumentValidator();
		documentIValidator.validate(goodDocument);
	}

	@Test(expected = DocumentAccessException.class)
	public void validateWithNullChapters() throws Exception {
		IValidator<Document> documentIValidator = new DocumentValidator();
		documentIValidator.validate(nullChaptersDocument);
	}

	@Test(expected = DocumentAccessException.class)
	public void validateWithEmptyChapters() throws Exception {
		IValidator<Document> documentIValidator = new DocumentValidator();
		documentIValidator.validate(emptyChaptersDocument);
	}

}