package by.epam.training.service.impl;

import by.epam.training.entity.Chapter;
import by.epam.training.entity.Document;
import by.epam.training.exception.ChapterNotFoundException;
import by.epam.training.exception.DocumentNotFoundException;
import by.epam.training.exception.DocumentServiceException;
import by.epam.training.logic.IUpdater;
import by.epam.training.logic.IValidator;
import by.epam.training.logic.impl.ChapterUpdateImpl;
import by.epam.training.logic.impl.DocumentUpdaterImpl;
import by.epam.training.logic.impl.DocumentValidator;
import by.epam.training.repository.MemoryDocumentRepositoryImpl;
import by.epam.training.service.IService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Arrays;

import static org.mockito.Mockito.*;

public class DocumentServiceImplTest {
	@Mock
	private MemoryDocumentRepositoryImpl memoryRepository = mock(MemoryDocumentRepositoryImpl.class);
	@Mock
	private
	IUpdater<Document> documentUpdater = new DocumentUpdaterImpl();
	@Mock
	private
	IUpdater<Chapter> chapterUpdater = new ChapterUpdateImpl();
	@Mock
	private
	IValidator<Document> documentIValidator = mock(DocumentValidator.class);
	@Mock
	private
	DocumentServiceImpl service = new DocumentServiceImpl(memoryRepository, documentUpdater, chapterUpdater, documentIValidator);
	private static final Document fakeDocument = new Document(
			0L,
			"TestDocument",
			Arrays.asList(
					new Chapter(
							3L,
							1,
							38
					),
					new Chapter(
							4L,
							2,
							17
					),
					new Chapter(
							5L,
							3,
							41
					)
			)
	);

	private static final Document noChartersToSetDocument = new Document(
			0L,
			"TestDocument",
			null
	);
	private static final Document document = new Document(
			0L,
			"TestDocument",
			Arrays.asList(
					new Chapter(
							0L,
							1,
							38
					),
					new Chapter(
							1L,
							2,
							17
					),
					new Chapter(
							2L,
							3,
							41
					)
			)
	);

	@Test
	public void defaultCtorCall() throws Exception {
		IService<Document> documentIService = new DocumentServiceImpl();
		Assert.assertNotNull(documentIService);
	}

	@Test
	public void create() throws Exception {
		service.create(document);
		verify(memoryRepository).create(document);
	}

	@Test(expected = DocumentNotFoundException.class)
	public void readNotFount() throws Exception {
		service.read(document.getId());
		verify(memoryRepository).read(document.getId());
	}

	@Test
	public void read() throws Exception {
		when(memoryRepository.read(document.getId())).thenReturn(document);
		service.read(document.getId());
		verify(memoryRepository).read(document.getId());
	}

	@Test
	public void update() throws Exception {
		when(memoryRepository.read(document.getId())).thenReturn(document);

		service.update(document.getId(), document);
		verify(memoryRepository).update(document);

	}
	@Test
	public void updateDocument() throws Exception {
		when(memoryRepository.read(document.getId())).thenReturn(document);
		service.update(document.getId(), noChartersToSetDocument);
		verify(memoryRepository).read(document.getId());
	}

	@Test(expected = DocumentNotFoundException.class)
	public void updateDocumentNotFount() throws Exception {
		service.update(document.getId(), noChartersToSetDocument);
		verify(memoryRepository).read(document.getId());
	}

	@Test
	public void delete() throws Exception {
		service.delete(document.getId());
		verify(memoryRepository).delete(document.getId());
	}

	@Test
	public void getAll() {

		service.getAll();
		verify(memoryRepository).getAll();
	}

	@Test
	public void put() throws Exception {
		service.put(document.getId(), document);
		verify(memoryRepository).update(document);
	}

	@Test(expected = ChapterNotFoundException.class)
	public void updateChapterException() throws Exception {

		when(memoryRepository.read(document.getId())).thenReturn(fakeDocument);

		Chapter expected = document.getChapters().get(2);
		service.updateChapter(0L, 0L, expected);
	}

	@Test
	public void updateChapter() throws Exception {

		when(memoryRepository.read(0L)).thenReturn(document);

		Chapter expected = document.getChapters().get(2);

		service.updateChapter(0L, 0L, expected);
		Assert.assertEquals(expected.getChapterNumber(), document.getChapters().get(0).getChapterNumber());
		Assert.assertEquals(expected.getNumberOfPages(), document.getChapters().get(0).getNumberOfPages());
	}
}