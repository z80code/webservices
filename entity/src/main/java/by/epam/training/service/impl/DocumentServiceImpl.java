package by.epam.training.service.impl;

import by.epam.training.entity.Chapter;
import by.epam.training.entity.Document;
import by.epam.training.exception.ChapterNotFoundException;
import by.epam.training.exception.DocumentAccessException;
import by.epam.training.exception.DocumentNotFoundException;
import by.epam.training.exception.DocumentServiceException;
import by.epam.training.logic.IValidator;
import by.epam.training.repository.IRepository;
import by.epam.training.service.IChapterService;
import by.epam.training.service.IService;
import by.epam.training.logic.IUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class DocumentServiceImpl implements IService<Document>, IChapterService {

	private IRepository<Document> repository;
	private IUpdater<Document> documentUpdater;
	private IUpdater<Chapter> chapterUpdater;
	private IValidator<Document> documentIValidator;

	public DocumentServiceImpl() {
	}

	@Autowired
	public DocumentServiceImpl(IRepository<Document> repository,
	                           IUpdater<Document> documentUpdater,
	                           IUpdater<Chapter> chapterUpdater,
	                           IValidator<Document> documentIValidator) {
		this.repository = repository;
		this.documentUpdater = documentUpdater;
		this.chapterUpdater = chapterUpdater;
		this.documentIValidator = documentIValidator;
	}

	@Override
	public Document create(Document document) {
		documentIValidator.validate(document);
		return repository.create(document);
	}

	@Override
	public Document read(Long id) {
		Document result = repository.read(id);
		if (result == null) {
			throw new DocumentNotFoundException("No a document with ID: " + id);
		}
		return result;
	}

	@Override
	public void update(Long id, Document document) {
		documentIValidator.validate(document);
		Document oldDocument = repository.read(id);
		if (oldDocument == null) {
			throw new DocumentNotFoundException("No a document with ID: " + id);
		}
		try {
			Document updatedDocument = documentUpdater.update(oldDocument, document);
			repository.update(updatedDocument);
		} catch (DocumentAccessException e) {
			throw new DocumentServiceException(e.getMessage(), e);
		}
	}

	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Override
	public Collection<Document> getAll() {
		return repository.getAll();
	}

	@Override
	public Document put(Long id, Document document) {
		documentIValidator.validate(document);
		document.setId(id);
		repository.update(document);
		return document;
	}

	@Override
	public void updateChapter(Long documentId, Long chapterId, Chapter chapter) {
		Document oldDocument = repository.read(documentId);
		if (oldDocument == null) {
			throw new DocumentNotFoundException("No a document with ID: " + documentId);
		}
		List<Chapter> chapters = oldDocument.getChapters();

		if (chapters.stream().noneMatch(ch -> Objects.equals(ch.getId(), chapterId))) {
			throw new ChapterNotFoundException("Chapter with ID like this not found in this document.");
		}

		oldDocument.setChapters(chapters.stream()
				.map(
						ch -> (Objects.equals(ch.getId(), chapterId)) ?
								chapterUpdater.update(ch, chapter) :
								ch).collect(Collectors.toList())
		);
	}
}
