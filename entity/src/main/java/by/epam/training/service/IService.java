package by.epam.training.service;

import by.epam.training.entity.Document;
import by.epam.training.exception.DocumentAccessException;

import java.util.Collection;

public interface IService<T> {

	T create(T item);

	T read(Long id);

	void update(Long id, T item) throws DocumentAccessException;

	void delete(Long id);

	Collection<Document> getAll();

	T put(Long id, Document item);
}
