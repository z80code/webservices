package by.epam.training.service;

import by.epam.training.entity.Chapter;

/**
 * Implement method for updating particular Chapter of Document.
 */

public interface IChapterService {
	void updateChapter(Long documentId, Long chapterId, Chapter chapter);
}