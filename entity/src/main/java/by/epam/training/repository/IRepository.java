package by.epam.training.repository;

import by.epam.training.entity.Document;

import java.util.Collection;

public interface IRepository<T> {

	T create(T item);

	T read(Long id);

	void update(T item);

	void delete(Long id);

	Collection<Document> getAll();
}
