package by.epam.training.repository;

import by.epam.training.entity.Document;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class MemoryDocumentRepositoryImpl implements IRepository<Document> {

	private AtomicLong indexesCounter = new AtomicLong(0);

	private Map<Long, Document> memoryRepository = new HashMap<>();

	public MemoryDocumentRepositoryImpl() {
	}

	public MemoryDocumentRepositoryImpl(Map<Long, Document> memoryRepository) {
		this.memoryRepository = memoryRepository;
	}

	@Override
	synchronized public Document create(Document item) {
		item.setId(indexesCounter.getAndIncrement());
		memoryRepository.put(item.getId(), item);
		return item;
	}

	@Override
	synchronized public Document read(Long id) {
		return memoryRepository.get(id);
	}

	@Override
	synchronized public void update(Document item) {
		memoryRepository.put(item.getId(), item);
	}

	@Override
	synchronized public void delete(Long id) {
		memoryRepository.remove(id);
	}

	@Override
	public Collection<Document> getAll() {
		return ((HashMap<Long, Document>) memoryRepository).values();
	}

	public AtomicLong getIndexesCounter() {
		return indexesCounter;
	}

	public void setIndexesCounter(AtomicLong indexesCounter) {
		this.indexesCounter = indexesCounter;
	}
}
