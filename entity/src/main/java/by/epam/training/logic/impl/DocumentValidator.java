package by.epam.training.logic.impl;

import by.epam.training.entity.Document;
import by.epam.training.exception.DocumentAccessException;
import by.epam.training.logic.IValidator;
import org.springframework.stereotype.Component;

@Component
public class DocumentValidator implements IValidator<Document> {

	@Override
	public void validate(Document document) {
		if (document.getChapters() == null || document.getChapters().size() == 0) {
			throw new DocumentAccessException("Your haven`t a right to deleting all Chapters of Document");
		}
	}
}
