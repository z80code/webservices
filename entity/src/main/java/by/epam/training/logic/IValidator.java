package by.epam.training.logic;

public interface IValidator<T> {
	void validate(T object);
}
