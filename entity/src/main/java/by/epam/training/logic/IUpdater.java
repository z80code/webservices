package by.epam.training.logic;

public interface IUpdater<D>{
	D update(D oldValue, D newValue);
}
