package by.epam.training.logic.impl;

import by.epam.training.entity.Chapter;
import by.epam.training.entity.Document;
import by.epam.training.exception.ChapterNotFoundException;
import by.epam.training.exception.DocumentAccessException;
import by.epam.training.logic.IUpdater;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class DocumentUpdaterImpl implements IUpdater<Document> {

	@Override
	public Document update(Document oldValue, Document newValue) {
		if (oldValue == null || newValue == null) {
			throw new IllegalArgumentException();
		}

		Document resultDocument = new Document();

		resultDocument.setId(oldValue.getId());
		if (newValue.getName() != null) {
			resultDocument.setName(newValue.getName());
		} else {
			resultDocument.setName(oldValue.getName());
		}

		if (newValue.getChapters() == null) {
			resultDocument.setChapters(oldValue.getChapters());
		} else {
			resultDocument.setChapters(newValue.getChapters());
		}
		return resultDocument;
	}
}
