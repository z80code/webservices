package by.epam.training.logic.impl;

import by.epam.training.entity.Chapter;
import by.epam.training.logic.IUpdater;
import org.springframework.stereotype.Component;

@Component
public class ChapterUpdateImpl implements IUpdater<Chapter> {

	@Override
	public Chapter update(Chapter oldChapter, Chapter newChapter) {
		if(oldChapter==null){
			throw new IllegalArgumentException("A Chapter not found.");
		}
		if(newChapter==null){
			throw new IllegalArgumentException("The chapter not set.");
		}
		Chapter resultChapter = new Chapter();

		resultChapter.setId(oldChapter.getId());
		if (newChapter.getChapterNumber() != null) {
			resultChapter.setChapterNumber(newChapter.getChapterNumber());
		} else {
			resultChapter.setChapterNumber(oldChapter.getChapterNumber());
		}

		if (newChapter.getNumberOfPages() != null) {
			resultChapter.setNumberOfPages(newChapter.getNumberOfPages());
		} else {
			resultChapter.setNumberOfPages(oldChapter.getNumberOfPages());
		}
		return resultChapter;
	}
}
