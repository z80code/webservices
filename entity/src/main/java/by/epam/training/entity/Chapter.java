package by.epam.training.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@XmlRootElement(name = "chapter")
@XmlAccessorType(XmlAccessType.FIELD)
public class Chapter {

	private Long id;
	private Integer chapterNumber;
	private Integer numberOfPages;

	public Chapter() {
	}

	public Chapter(Long id, Integer chapterNumber, Integer numberOfPages) {
		this.id = id;
		this.chapterNumber = chapterNumber;
		this.numberOfPages = numberOfPages;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getChapterNumber() {
		return chapterNumber;
	}

	public void setChapterNumber(Integer chapterNumber) {
		this.chapterNumber = chapterNumber;
	}

	public Integer getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(Integer numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Chapter)) return false;
		Chapter chapter = (Chapter) o;
		return Objects.equals(getId(), chapter.getId()) &&
				Objects.equals(getChapterNumber(), chapter.getChapterNumber()) &&
				Objects.equals(getNumberOfPages(), chapter.getNumberOfPages());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getChapterNumber(), getNumberOfPages());
	}

	@Override
	public String toString() {
		return "Chapter{" +
				"id=" + id +
				", chapterNumber=" + chapterNumber +
				", numberOfPages=" + numberOfPages +
				'}';
	}
}
