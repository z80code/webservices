package by.epam.training.entity;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

@XmlRootElement(name = "document")
@XmlAccessorType(XmlAccessType.FIELD)
public class Document {

	private Long id;
	private String name;
	@XmlElementWrapper(name = "chapters")
	@XmlElement(name = "chapter")
	private List<Chapter> chapters;

	public Document() {
	}

	public Document(Long id, String name, List<Chapter> chapters) {
		this.id = id;
		this.name = name;
		this.chapters = chapters;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Chapter> getChapters() {
		return chapters;
	}

	public void setChapters(List<Chapter> chapters) {
		this.chapters = chapters;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Document)) return false;
		Document document = (Document) o;
		return Objects.equals(getId(), document.getId()) &&
				Objects.equals(getName(), document.getName()) &&
				document.getChapters().stream().allMatch(ch -> getChapters().contains(ch) &&
						document.getChapters().size() == getChapters().size()
				);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getName(), getChapters());
	}

	@Override
	public String toString() {
		return "Document{" +
				"id=" + id +
				", name='" + name + '\'' +
				", chapters=" + chapters +
				'}';
	}
}
