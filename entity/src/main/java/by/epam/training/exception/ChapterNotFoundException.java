package by.epam.training.exception;

public class ChapterNotFoundException extends ResourceNotFoundException {
	public ChapterNotFoundException() {
	}

	public ChapterNotFoundException(String message) {
		super(message);
	}

	public ChapterNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
