package by.epam.training.exception;

public class DocumentAccessException extends DocumentException {

	public DocumentAccessException() {
	}

	public DocumentAccessException(String s) {
		super(s);
	}
}
