package by.epam.training.exception;

public class DocumentNotFoundException extends ResourceNotFoundException {
	public DocumentNotFoundException() {
	}

	public DocumentNotFoundException(String message) {
		super(message);
	}

	public DocumentNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
