package by.epam.training.exception;

public class DocumentServiceException extends DocumentException {
	public DocumentServiceException() {
	}

	public DocumentServiceException(String message) {
		super(message);
	}

	public DocumentServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
