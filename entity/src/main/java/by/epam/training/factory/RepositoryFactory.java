package by.epam.training.factory;

import by.epam.training.entity.Chapter;
import by.epam.training.entity.Document;
import by.epam.training.repository.IRepository;
import by.epam.training.repository.MemoryDocumentRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Component
public final class RepositoryFactory {
	@Bean
	public IRepository<Document> getDocumentRepository() {
		/*
		 * This methods return an object of the DocumentRepository
		 * and this one can return FakeDocumentRepository for this task requirements.
		 * Witch one would be returned can be written in properties file.
		 * And now it`s MemoryDocumentRepositoryImpl.
		 */
		// Warning! HardCode!
		Map<Long, Document> staticInitDataDocuments = new HashMap<Long, Document>() {{
			put(
					0L,
					new Document(
							0L,
							"Report",
							Arrays.asList(
									new Chapter(
											0L,
											1,
											20
									),
									new Chapter(
											1L,
											2,
											35
									),
									new Chapter(
											2L,
											3,
											15
									)

							)
					));
			put(
					1L,
					new Document(
							1L,
							"SubReport",
							Arrays.asList(
									new Chapter(
											3L,
											1,
											16
									),
									new Chapter(
											4L,
											2,
											62
									),
									new Chapter(
											5L,
											3,
											23
									)

							)
					)
			);
			put(
					2L,
					new Document(
							2L,
							"SubSubReport",
							Arrays.asList(
									new Chapter(
											6L,
											1,
											38
									),
									new Chapter(
											7L,
											2,
											17
									),
									new Chapter(
											8L,
											3,
											41
									)
							)
					)
			);
		}};
		MemoryDocumentRepositoryImpl memoryDocumentRepository =	new MemoryDocumentRepositoryImpl(staticInitDataDocuments);
		memoryDocumentRepository.setIndexesCounter(new AtomicLong(3));
		return memoryDocumentRepository;
	}
}
