package by.epam.training;

import by.epam.training.entity.Chapter;
import by.epam.training.entity.Document;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * !!! Attention !!!
 * All local messages can be rewritten as a logging.
 */
public class RestfulClient {

	private RestTemplate restTemplate;

	private static final String BASE_URI = "http://localhost:8080/api/rest/documents/";

	public RestfulClient() {
		restTemplate = new RestTemplate();
	}

	//	@GET
	//	@Path("/")
	//	Response getAllDocuments();
	public List<Document> getAllEntity() {
		ResponseEntity<List<Document>> getResponse =
				restTemplate.exchange(BASE_URI,
						HttpMethod.GET,
						null,
						new ParameterizedTypeReference<List<Document>>() {
						});
		return getResponse.getBody();
	}

	//	@GET
	//	@Path("/{documentId}")
	//	Response getDocument(@PathParam("documentId") Long id);
	public Document getEntity(Long documentId) {
		ResponseEntity<Document> getResponse =
				restTemplate.getForEntity(BASE_URI + documentId, Document.class);
		return getResponse.getBody();
	}

	//	@POST
	//	@Path("/")
	//	Response addDocument(Document document);
	public Document postEntity(Document document) {
		ResponseEntity<Document> postResponse =
				restTemplate.postForEntity(BASE_URI, document, Document.class);
		return postResponse.getBody();
	}

	//	@PUT
	//	@Path("/")
	//	Response putDocument();
	public void putEntity(Document document) {
		restTemplate.put(BASE_URI, document);
	}

	//	@PUT
	//	@Path("/{documentId}")
	//	Response putDocument(@PathParam("documentId") Long id, Document document);
	public void putEntityReplace(Long documentId, Document document) {
		restTemplate.put(BASE_URI + documentId, document);
	}

	//  :(( Not supported.
	//	@PATCH
	//	@Path("/{documentId}")
	//	Response updateDocument(@PathParam("documentId") Long id, Document document);
//	public void patchEntity(Long documentId, Document document) {
//		restTemplate.patchForObject(BASE_URI+documentId, document, Document.class);
//	}

	//	@DELETE
	//	@Path("/{documentId}")
	//	Response deleteDocument(@PathParam("documentId") Long id);
	public void deleteEntity(Long documentId) {
		restTemplate.delete(BASE_URI + documentId);
	}

	//	@PUT
	//	@Path("/{documentId}/chapters/{chapterId}")
	//	Response updateChapter(@PathParam("documentId") Long documentId, @PathParam("chapterId") Long chapterId, Chapter chapter);
	public void putEntityChapter(Long documentId, Long chapterId, Chapter chapter) {
		String url = BASE_URI + documentId + "/chapters/" + chapterId;
		restTemplate.put(url, chapter);
	}

}
