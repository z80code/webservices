import by.epam.training.entity.Chapter;
import by.epam.training.entity.Document;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;

import by.epam.training.RestfulClient;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableAutoConfiguration(
		exclude = {
				EmbeddedServletContainerAutoConfiguration.class,
				WebMvcAutoConfiguration.class})
public class RestClientRunner {

	public static void main(String[] args) {

		RestfulClient restfulClient = new RestfulClient();
		try {
			//	@GET (all)
			System.out.println("Begin /GET all request!");
			List<Document> list = restfulClient.getAllEntity();
			if (list != null) {
				System.out.println("Response for Get Request: " + list);
			} else {
				System.out.println("Response for Get Request: NULL");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			//	@GET
			System.out.println("Begin /GET request!");
			Document document = restfulClient.getEntity(0L);
			if (document != null) {
				System.out.println("Response for Get Request: " + document);
			} else {
				System.out.println("Response for Get Request: NULL");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			//	@POST
			System.out.println("Begin /POST request!");
			Document document = restfulClient.postEntity(testDocument);
			System.out.println("Response for Post Request: " + document);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			//	@PUT
			System.out.println("Begin /PUT request!");
			restfulClient.putEntity(testDocument);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			//	@PUT
			System.out.println("Begin /PUT request(replace)!");
			restfulClient.putEntityReplace(0L, testDocument);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		/**
		 * additional task section
		 */
		try {
			//	@PUT
			System.out.println("Begin /PUT request(task)!");
			restfulClient.putEntityReplace(0L, expectedByChaptersIsNull);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		/**
		 * end task section
		 */
		try {
			//	@DELETE
			System.out.println("Begin /DELETE request!");
			restfulClient.deleteEntity(0L);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			//	@PUT
			System.out.println("Begin /PUT request for Chapter!");
			restfulClient.putEntityChapter(1L, 3L, chapter);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static final Document expectedByChaptersIsNull = new Document(
			0L,
			"Document",
			null
	);

	private static final Document testDocument = new Document(
			0L,
			"TestDocument",
			Arrays.asList(
					new Chapter(
							9L,
							1,
							38
					),
					new Chapter(
							10L,
							2,
							17
					),
					new Chapter(
							11L,
							3,
							41
					)
			)
	);

	private static final Chapter chapter = new Chapter(
			10L,
			20,
			40
	);
}